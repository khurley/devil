//
//  Little cms
//  Copyright (C) 1998-2001 Marti Maria
//
// THIS SOFTWARE IS PROVIDED "AS-IS" AND WITHOUT WARRANTY OF ANY KIND,
// EXPRESS, IMPLIED OR OTHERWISE, INCLUDING WITHOUT LIMITATION, ANY
// WARRANTY OF MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE.
//
// IN NO EVENT SHALL MARTI MARIA BE LIABLE FOR ANY SPECIAL, INCIDENTAL,
// INDIRECT OR CONSEQUENTIAL DAMAGES OF ANY KIND,
// OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS,
// WHETHER OR NOT ADVISED OF THE POSSIBILITY OF DAMAGE, AND ON ANY THEORY OF
// LIABILITY, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE
// OF THIS SOFTWARE.
//
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

#include "lcms.h"




/*
       This module provides conversion stages for handling intents.

The chain of evaluation in a transform is:

                PCS1            PCS2                    PCS3          PCS4

|From |  |From  |  |Conversion |  |Preview |  |Gamut   |  |Conversion |  |To    |  |To     |
|Input|->|Device|->|Stage 1    |->|handling|->|Checking|->|Stage 2    |->|Device|->|output |

--------  -------  -------------   ---------  ----------  -------------   -------  ---------

          AToB0                     prew0       gamut                     BToA0
Formatting LUT      Adjusting        LUT         LUT       Adjusting       LUT      Formatting
          Intent     Intent 1       intent      intent      Intent 2      Intent


Some of these LUT may be missing

There are two intents involved here, the intent of the transform itself, and the
intent the proof is being done, if is the case. Since the first intent is to be
applied to preview, is the proofing intent. The second intent  identifies the
transform intent. Input data of any stage is taked as relative colorimetric
always.

*/

/*
       Conversion between Absolute/relativ Lab, XYZ


#define XYZRel       0
#define LabRel       1
#define XYZAbs       2
#define LabAbs       3



typedef void (* _cmsADJFN)(WORD In[], WORD Out[], LPWVEC3 a, LPWVEC3 b);
*/


int cdecl cmsChooseCnvrt(int Absolute,
                 int Phase1, LPcmsCIEXYZ BlackPointIn,
                             LPcmsCIEXYZ WhitePointIn,
                             LPcmsCIEXYZ IlluminantIn,

                 int Phase2, LPcmsCIEXYZ BlackPointOut,
                             LPcmsCIEXYZ WhitePointOut,
                             LPcmsCIEXYZ IlluminantOut,

                 _cmsADJFN *fn1,
                 LPWVEC3 wa, LPWVEC3 wb);


// -------------------------------------------------------------------------

// D50 - Widely used

LCMSAPI LPcmsCIEXYZ LCMSEXPORT cmsD50_XYZ(void)
{
	static cmsCIEXYZ D50XYZ = {D50X, D50Y, D50Z};

	return &D50XYZ;
}

LCMSAPI LPcmsCIExyY LCMSEXPORT cmsD50_xyY(void)
{
	static cmsCIExyY D50xyY = {D50x, D50y, D50Y};

	return &D50xyY;
}


// ---------------- From LUT to LUT --------------------------


// Calculate a, b for Relativ -> Absolute
//
//     Out = BP+(WP-BP)/Ill*In
//
//      b = MediaBlack
//     a = (MediaWhite - MediaBlack) / Illuminant
//

static
void Rel2AbsCoefs(LPcmsCIEXYZ BlackPoint,
                  LPcmsCIEXYZ WhitePoint,
                  LPcmsCIEXYZ Illuminant,
                  LPVEC3 a, LPVEC3 b)
{

       VEC3init(b, -BlackPoint->X, -BlackPoint->Y, -BlackPoint->Z);

       VEC3init(a, (WhitePoint->X - BlackPoint->X) / Illuminant -> X,
                   (WhitePoint->Y - BlackPoint->Y) / Illuminant -> Y,
                   (WhitePoint->Z - BlackPoint->Z) / Illuminant -> Z);
}

// Calculate a, b for Absolute -> Relativ
//
//  Out = (In - BP) * Ill / (WP - BP) == Ill/(WP-BP) * In - BP*Ill/(WP-BP)
//

static
void Abs2RelCoefs(LPcmsCIEXYZ BlackPoint,
                  LPcmsCIEXYZ WhitePoint,
                  LPcmsCIEXYZ Illuminant,
                  LPVEC3 a, LPVEC3 b)
{
       double tx, ty, tz;

       tx = Illuminant -> X / (WhitePoint->X - BlackPoint->X);
       ty = Illuminant -> Y / (WhitePoint->Y - BlackPoint->Y);
       tz = Illuminant -> Z / (WhitePoint->Z - BlackPoint->Z);

       VEC3init(a, tx, ty, tz);

       VEC3init(b, -tx * BlackPoint->X,
                   -ty * BlackPoint->Y,
                   -tz * BlackPoint->Z);
}

// join scalings to obtain:
//     relative input to absolute and then to relative output

static
void Rel2RelStepAbsCoefs(LPcmsCIEXYZ BlackPointIn,
                      LPcmsCIEXYZ WhitePointIn,
                      LPcmsCIEXYZ IlluminantIn,
                      LPcmsCIEXYZ BlackPointOut,
                      LPcmsCIEXYZ WhitePointOut,
                      LPcmsCIEXYZ IlluminantOut,
                      LPVEC3 a, LPVEC3 b)
{
       VEC3 ain, bin, aout, bout;


       Rel2AbsCoefs(BlackPointIn,
                    WhitePointIn,
                    IlluminantIn, &ain, &bin);

       Abs2RelCoefs(BlackPointOut,
                    WhitePointOut,
                    IlluminantOut, &aout, &bout);


       VEC3init(a, aout.n[VX]*ain.n[VX],
                   aout.n[VY]*ain.n[VY],
                   aout.n[VZ]*ain.n[VZ]);

       VEC3init(b, (aout.n[VX]*bin.n[VX] + bout.n[VX]),
                   (aout.n[VY]*bin.n[VY] + bout.n[VY]),
                   (aout.n[VZ]*bin.n[VZ] + bout.n[VZ]));

}



// Calculate scaling between illuminants

static
void BetweenIlluminants(LPcmsCIEXYZ IlluminantFrom, LPcmsCIEXYZ IlluminantTo, LPVEC3 a)
{

       VEC3init(a, IlluminantTo->X/IlluminantFrom->X,
                   IlluminantTo->Y/IlluminantFrom->Y,
                   IlluminantTo->Z/IlluminantFrom->Z);
}

// Calculate scaling from illuminant to D50

static
void ToD50(LPcmsCIEXYZ Illuminant, LPVEC3 a)
{
       VEC3init(a, D50X/Illuminant->X, D50Y/Illuminant->Y,D50Z/Illuminant->Z);
}

// Calculate scaling from D50 to illuminant

static
void FromD50(LPcmsCIEXYZ Illuminant, LPVEC3 a)
{
       VEC3init(a, Illuminant->X/D50X, Illuminant->Y/D50Y,Illuminant->Z/D50Z);
}


// Simpler identity (not used)


#ifdef __BORLANDC__
#pragma warn -par
#endif

#ifdef _MSC_VER
#pragma warning(disable : 4100 4505)
#endif

static
void Identity(WORD In[], WORD Out[], LPWVEC3 a, LPWVEC3 b)
{
#ifdef __MWERKS__
#pragma unused(a,b)
#endif
       Out[0] = In[0];
       Out[1] = In[1];
       Out[2] = In[2];
}


// XYZ to XYZ linear scalling

static
void XYZ2XYZ(WORD In[], WORD Out[], LPWVEC3 a, LPWVEC3 b)
{
  Out[0] = Clamp_XYZ((b->n[0] + FixedMul(a->n[0], ((Fixed32) In[0] << 1)) ) >> 1);
  Out[1] = Clamp_XYZ((b->n[1] + FixedMul(a->n[1], ((Fixed32) In[1] << 1)) ) >> 1);
  Out[2] = Clamp_XYZ((b->n[2] + FixedMul(a->n[2], ((Fixed32) In[2] << 1)) ) >> 1);
}


// XYZ to Lab, scaling first

static
void XYZ2Lab(WORD In[], WORD Out[], LPWVEC3 a, LPWVEC3 b)
{
  WORD XYZ[3];

  XYZ2XYZ(In, XYZ, a, b);
  cmsXYZ2LabEncoded(XYZ, Out);
}

// Lab to XYZ, then scalling

static
void Lab2XYZ(WORD In[], WORD Out[], LPWVEC3 a, LPWVEC3 b)
{
       WORD XYZ[3];

       cmsLab2XYZEncoded(In, XYZ);
       XYZ2XYZ(XYZ, Out, a, b);
}

// Lab to XYZ, scalling and then, back to Lab

static
void Lab2XYZ2Lab(WORD In[], WORD Out[], LPWVEC3 a, LPWVEC3 b)
{
       WORD XYZ[3], XYZ2[3];

       cmsLab2XYZEncoded(In, XYZ);
       XYZ2XYZ(XYZ, XYZ2, a, b);
       cmsXYZ2LabEncoded(XYZ2, Out);
}



// Dispatcher for XYZ Relative LUT

static
int FromXYZRelLUT(int Absolute,
                             LPcmsCIEXYZ BlackPointIn,
                             LPcmsCIEXYZ WhitePointIn,
                             LPcmsCIEXYZ IlluminantIn,

                 int Phase2, LPcmsCIEXYZ BlackPointOut,
                             LPcmsCIEXYZ WhitePointOut,
                             LPcmsCIEXYZ IlluminantOut,

                 _cmsADJFN *fn1,
                 LPVEC3 a, LPVEC3 b)

{
              switch (Phase2) {

                     // From relative XYZ to Relative XYZ.

                     case XYZRel:

                            if (Absolute)
                            {
                                   // From input relative to absolute, and then
                                   // back to output relative

                                   Rel2RelStepAbsCoefs(BlackPointIn,
                                                  WhitePointIn,
                                                  IlluminantIn,
                                                  BlackPointOut,
                                                  WhitePointOut,
                                                  IlluminantOut,
                                                  a, b);
                                   *fn1 = XYZ2XYZ;

                            }
                            else
                            {
                                   // Relative to relative, only
                                   // adjust the illuminants

                                   BetweenIlluminants(IlluminantIn,
                                                      IlluminantOut, a);
                                   VEC3init(b, 0, 0, 0);
                                   *fn1 = XYZ2XYZ;
                            }
                            break;

                     // From relative XYZ to absolute XYZ. Always absolute

                     case XYZAbs:

                            Rel2AbsCoefs(BlackPointIn, WhitePointIn, IlluminantIn, a, b);
                            *fn1 = XYZ2XYZ;
                            break;

                     // From relative XYZ to Relative Lab

                     case LabRel:

                            // First pass XYZ to absolute, then to relative and
                            // finally to Lab. I use here D50 for output in order
                            // to prepare the "to Lab" conversion.

                            if (Absolute)
                            {
                                VEC3 a1, b1, btw;

                                Rel2RelStepAbsCoefs(BlackPointIn,
                                                    WhitePointIn,
                                                    IlluminantIn,
                                                    BlackPointOut,
                                                    WhitePointOut,
                                                    IlluminantOut,
                                                    &a1, &b1);

                                BetweenIlluminants(IlluminantIn,
                                                   cmsD50_XYZ(),
                                                   &btw);

                                VEC3perComp(a, &a1, &btw);
                                VEC3perComp(b, &b1, &btw);
                                *fn1 = XYZ2Lab;

                            }
                            else
                            {
                                   // Only adjust illuminant to D50

                                   BetweenIlluminants(IlluminantIn,
                                                      cmsD50_XYZ(), a);
                                   VEC3init(b, 0, 0, 0);
                                   *fn1 = XYZ2Lab;
                            }
                            break;

                     // From relative XYZ To Absolute Lab, adjusting to D50

                     case LabAbs:
                                {
                                VEC3 a1, b1, btw;

                                Rel2RelStepAbsCoefs(BlackPointIn,
                                                    WhitePointIn,
                                                    IlluminantIn,
                                                    BlackPointOut,
                                                    WhitePointOut,
                                                    IlluminantOut,
                                                    &a1, &b1);

                                BetweenIlluminants(IlluminantOut,
                                                   cmsD50_XYZ(),
                                                   &btw);

                                VEC3perComp(a, &a1, &btw);
                                VEC3perComp(b, &b1, &btw);
                                *fn1 = XYZ2Lab;
                                }
                            break;


                     default: return FALSE;
                     }

              return TRUE;
}


// Since XYZ comes in absolute colorimetry, no endpoints on input
// are needed.

static
int FromXYZAbsLUT(
                 int Phase2, LPcmsCIEXYZ BlackPointOut,
                             LPcmsCIEXYZ WhitePointOut,
                             LPcmsCIEXYZ IlluminantOut,

                 _cmsADJFN *fn1,
                 LPVEC3 a, LPVEC3 b)

{

          switch (Phase2) {

              case XYZRel:
                     Abs2RelCoefs(BlackPointOut, WhitePointOut, IlluminantOut, a, b);
                     *fn1 = XYZ2XYZ;
                     break;

              case XYZAbs:         // Identity
                     *fn1 = NULL;
                     break;

              case LabRel:
                     Abs2RelCoefs(BlackPointOut, WhitePointOut, IlluminantOut, a, b);
                     *fn1 = XYZ2Lab;
                     break;

              case LabAbs:
                     FromD50(IlluminantOut, a);
                     *fn1 = XYZ2Lab;
                     break;

              default: return FALSE;
              }
       return TRUE;
}


// From Lab Relative type LUT

static
int FromLabRelLUT(int Absolute,
                             LPcmsCIEXYZ BlackPointIn,
                             LPcmsCIEXYZ WhitePointIn,
                             LPcmsCIEXYZ IlluminantIn,

                 int Phase2, LPcmsCIEXYZ BlackPointOut,
                             LPcmsCIEXYZ WhitePointOut,
                             LPcmsCIEXYZ IlluminantOut,

                 _cmsADJFN *fn1,
                 LPVEC3 a, LPVEC3 b)
{

          switch (Phase2) {

              // From Lab Relative to XYZ Relative, very usual case

              case XYZRel:

                     if (Absolute)
                     {
                            // From lab relative, to XYZ absolute, and then,
                            // back to XYZ relative

                            Rel2RelStepAbsCoefs(BlackPointIn,
                                           WhitePointIn,
                                           cmsD50_XYZ(),
                                           BlackPointOut,
                                           WhitePointOut,
                                           IlluminantOut,
                                           a, b);

                            *fn1 = Lab2XYZ;

                     }
                     else
                     {
                            // From Lab relative, to XYZ absolute. It is
                            // needed to adjust illuminants, but since input
                            // is Lab, it is only necessary adjust from D50

                            FromD50(IlluminantOut, a);
                            VEC3init(b, 0, 0, 0);
                            *fn1 = Lab2XYZ;
                     }
                     break;


              // From Relative Lab to XYZ absolute. First covert to relative XYZ,
              // then to absolute XYZ

              case XYZAbs:
                     {
                     Rel2AbsCoefs(BlackPointIn, WhitePointIn, cmsD50_XYZ(), a, b);
                     *fn1 = Lab2XYZ;
                     }
                     break;

              case LabRel:
                     if (Absolute)
                     {
                     // First pass to XYZ using the input illuminant
                     // * InIlluminant / D50, then to absolute. Then
                     // to relative, but for input

                     Rel2RelStepAbsCoefs(BlackPointIn, WhitePointIn, IlluminantIn,
                                         BlackPointOut, WhitePointOut, cmsD50_XYZ(),
                                         a, b);
                     *fn1 = Lab2XYZ2Lab;
                     }
                     else
                     {
                     *fn1 = NULL;
                     }
                     break;

              case LabAbs:
                     Rel2AbsCoefs(BlackPointIn, WhitePointIn, cmsD50_XYZ(), a, b);
                     *fn1 = Lab2XYZ2Lab;
                     break;

              default: return FALSE;
              }

   return TRUE;
}


// From Lab Absolute LUT, always absolute

static
int FromLabAbsLUT(           LPcmsCIEXYZ BlackPointIn,
                             LPcmsCIEXYZ WhitePointIn,
                             LPcmsCIEXYZ IlluminantIn,

                 int Phase2, LPcmsCIEXYZ BlackPointOut,
                             LPcmsCIEXYZ WhitePointOut,
                             LPcmsCIEXYZ IlluminantOut,

                 _cmsADJFN *fn1,
                 LPVEC3 a, LPVEC3 b)
{


          switch (Phase2) {

              // From Lab absolute to XYZ Relative, convert first to
              // Absolute XYZ, then to relative

              case XYZRel:
                     Abs2RelCoefs(BlackPointIn, WhitePointIn, IlluminantIn, a, b);
                     ToD50(IlluminantOut, a);
                     *fn1 = Lab2XYZ;
                     break;

              case XYZAbs:
                     Abs2RelCoefs(BlackPointOut, WhitePointOut, IlluminantOut, a, b);
                     *fn1 = Lab2XYZ;
                     break;

              case LabRel:
                     Abs2RelCoefs(BlackPointOut, WhitePointOut, IlluminantOut, a, b);
                     *fn1 = Lab2XYZ2Lab;
                     break;

              case LabAbs:
                     *fn1 = NULL;
                     break;

              default: return FALSE;
              }

        return TRUE;
}


// This function does calculate the necessary conversion operations
// needed from transpassing data from a LUT to a LUT. The conversion
// is modeled as a pointer of function and two coefficients, a and b
// The function is actually called only if not null pointer is provided,
// and the two paramaters are passed in. There are several types of
// conversions, but basically they do a linear scalling and a interchange



// Main dispatcher

int cmsChooseCnvrt(int Absolute,
                 int Phase1, LPcmsCIEXYZ BlackPointIn,
                             LPcmsCIEXYZ WhitePointIn,
                             LPcmsCIEXYZ IlluminantIn,

                 int Phase2, LPcmsCIEXYZ BlackPointOut,
                             LPcmsCIEXYZ WhitePointOut,
                             LPcmsCIEXYZ IlluminantOut,

                 _cmsADJFN *fn1,
                 LPWVEC3 wa, LPWVEC3 wb)
{

       int rc;
       VEC3 a, b;


       VEC3init(&a, 1, 1, 1);
       VEC3init(&b, 0, 0, 0);

       switch (Phase1) {

       // Input LUT is giving XYZ relative values.

       case XYZRel:  rc = FromXYZRelLUT(Absolute,
                                          BlackPointIn,
                                          WhitePointIn,
                                          IlluminantIn,
                                          Phase2,
                                          BlackPointOut,
                                          WhitePointOut,
                                          IlluminantOut,
                                          fn1, &a, &b);
                     break;

       // Input LUT is giving XYZ Absolute values. Always absolute

       case XYZAbs:  rc = FromXYZAbsLUT(Phase2,
                                          BlackPointOut,
                                          WhitePointOut,
                                          IlluminantOut,
                                          fn1, &a, &b);
                     break;

       // Input LUT is giving Lab relative values

       case LabRel:  rc =  FromLabRelLUT(Absolute,
                                          BlackPointIn,
                                          WhitePointIn,
                                          IlluminantIn,
                                          Phase2,
                                          BlackPointOut,
                                          WhitePointOut,
                                          IlluminantOut,
                                          fn1, &a, &b);
                     break;

       // Input LUT is giving absolute Lab values, Always absolute

       case LabAbs:  rc = FromLabAbsLUT(BlackPointIn,
                                          WhitePointIn,
                                          IlluminantIn,
                                          Phase2,
                                          BlackPointOut,
                                          WhitePointOut,
                                          IlluminantOut,
                                          fn1, &a, &b);
                     break;

       // Unrecognized combination

       default:    cmsSignalError(LCMS_ERRC_ABORTED, "(internal) Phase error");
                   return FALSE;

       }

       VEC3toFix(wa, &a);
       VEC3toFix(wb, &b);

       return rc;
}
