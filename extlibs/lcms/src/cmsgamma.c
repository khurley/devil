//
//  Little cms
//  Copyright (C) 1998-2000 Marti Maria
//
// THIS SOFTWARE IS PROVIDED "AS-IS" AND WITHOUT WARRANTY OF ANY KIND,
// EXPRESS, IMPLIED OR OTHERWISE, INCLUDING WITHOUT LIMITATION, ANY
// WARRANTY OF MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE.
//
// IN NO EVENT SHALL MARTI MARIA BE LIABLE FOR ANY SPECIAL, INCIDENTAL,
// INDIRECT OR CONSEQUENTIAL DAMAGES OF ANY KIND,
// OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS,
// WHETHER OR NOT ADVISED OF THE POSSIBILITY OF DAMAGE, AND ON ANY THEORY OF
// LIABILITY, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE
// OF THIS SOFTWARE.
//
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

#include "lcms.h"

// Gamma handling. I'm encoding gamma as 0..ffff value

LPGAMMATABLE LCMSEXPORT cmsAllocGamma(int nEntries);
void         LCMSEXPORT cmsFreeGamma(LPGAMMATABLE Gamma);
LPGAMMATABLE LCMSEXPORT cmsBuildGamma(int nEntries, double Gamma);
LPGAMMATABLE LCMSEXPORT cmsReverseGamma(int nResultSamples, LPGAMMATABLE InGamma);
LPGAMMATABLE LCMSEXPORT cmsJoinGamma(LPGAMMATABLE InGamma, LPGAMMATABLE OutGamma);
LPGAMMATABLE LCMSEXPORT cmsJoinGammaEx(LPGAMMATABLE InGamma, LPGAMMATABLE OutGamma, int nPoints);
BOOL		 LCMSEXPORT cmsSmoothGamma(LPGAMMATABLE Tab, double lambda);


LPGAMMATABLE cdecl cmsScaleGamma(LPGAMMATABLE Input, Fixed32 Factor);


// Sampled curves

LPSAMPLEDCURVE cdecl cmsAllocSampledCurve(int nItems);
void           cdecl cmsFreeSampledCurve(LPSAMPLEDCURVE p);
void		   cdecl cmsEndpointsOfSampledCurve(LPSAMPLEDCURVE p, double* Min, double* Max);
void		   cdecl cmsClampSampledCurve(LPSAMPLEDCURVE p, double Min, double Max);
BOOL           cdecl cmsSmoothSampledCurve(LPSAMPLEDCURVE Tab, double SmoothingLambda);
void		   cdecl cmsRescaleSampledCurve(LPSAMPLEDCURVE p, double Min, double Max, int nPoints);

LPSAMPLEDCURVE cdecl cmsSmoothParametricCurve(LPSAMPLEDCURVE X, LPSAMPLEDCURVE Y,
												int nResultingPoints, double SmoothingLambda);


// ----------------------------------------------------------------------------------------
// default gamma function

// Implementing gamma as a two-stages funtion.
// I chose the cutoff between linear-exponential stage on
//
//     (breakin, breakout)=(0.081, 0.018)
//
// The linear segment minimizes the effect of sensor noise in
// practical devices.

#define PHOTOSHOP_COMPATIBILITY    1

#if 1 // def PHOTOSHOP_COMPATIBLITY --- Buggy 24/5/2000

static
double FGamma(double R, double x)
{
       return pow(R, x);
}

#else

static
double FGamma(double R, double x)
{
       double startin, endin;
       double startout, endout;
       double breakin, breakout;
       double a, c;


       startin = 0; endin = 1.0;
       startout= 0; endout = 1.0;

       breakin  = 0.081;
       breakout = 0.018;

       if (R >= startin && R <= breakin)
       {
              return (breakout-startout)*(R-startin)/(breakin-startin) + startout;
       }
       else
       {
              a = (endout-breakout)/(1-pow((breakin-startin)/(endin-startin), x));
              c = endout - a;

              return a*pow((R-startin)/(endin-startin), x)+c;
       }
}

#endif

LPGAMMATABLE LCMSEXPORT cmsAllocGamma(int nEntries)
{
       LPGAMMATABLE p;
       size_t size;

       size = sizeof(GAMMATABLE) + (sizeof(WORD) * (nEntries-1));

       p = (LPGAMMATABLE) malloc(size);
       if (!p) return NULL;

       p -> nEntries = nEntries;
       ZeroMemory(p -> GammaTable, nEntries * sizeof(WORD));

       return p;
}

void LCMSEXPORT cmsFreeGamma(LPGAMMATABLE Gamma)
{
       free(Gamma);
}




// Build a gamma table based on gamma constant

LPGAMMATABLE LCMSEXPORT cmsBuildGamma(int nEntries, double Gamma)
{
       LPGAMMATABLE p;
       LPWORD Table;
       int i;
       double R, Val;

       if (nEntries > 65530) {
                cmsSignalError(LCMS_ERRC_WARNING, "Couldn't create gammatable of more than 65530 entries; 65530 assumed");
                nEntries = 65530;
       }

       p = cmsAllocGamma(nEntries);
       if (!p) return NULL;

       Table = p -> GammaTable;
       if (Gamma == 0.0)
       {
              ZeroMemory(Table, nEntries*sizeof(WORD));
              return p;
       }


       for (i=0; i < nEntries; i++)
       {
              R   = (double) i / (nEntries-1);
              Val = FGamma(R, Gamma);

              Table[i] = (WORD) floor(Val * 0xFFFFL);
       }

       return p;
}


// Handle gamma using interpolation tables. The resulting curves can become
// very stange, but are pleasent to eye.

LPGAMMATABLE LCMSEXPORT cmsJoinGamma(LPGAMMATABLE InGamma,
                          LPGAMMATABLE OutGamma)
{
       register int i;
       L16PARAMS L16In, L16Out;
       LPWORD InPtr, OutPtr;
       LPGAMMATABLE p;

       p = cmsAllocGamma(256);
       if (!p) return NULL;

       cmsCalcL16Params(InGamma -> nEntries, &L16In);
       InPtr  = InGamma -> GammaTable;

       cmsCalcL16Params(OutGamma -> nEntries, &L16Out);
       OutPtr = OutGamma-> GammaTable;

       for (i=0; i < 256; i++)
       {
              WORD wValIn, wValOut;

              wValIn  = cmsLinearInterpLUT16(RGB_8_TO_16(i), InPtr, &L16In);
              wValOut = cmsReverseLinearInterpLUT16(wValIn, OutPtr, &L16Out);

              p -> GammaTable[i] = wValOut;
       }

       return p;
}



LPGAMMATABLE LCMSEXPORT cmsJoinGammaEx(LPGAMMATABLE InGamma,
                          LPGAMMATABLE OutGamma, int nPoints)
{
       register int i;
       L16PARAMS L16In, L16Out;
       LPWORD InPtr, OutPtr;
       LPGAMMATABLE p;

       p = cmsAllocGamma(nPoints);
       if (!p) return NULL;

       cmsCalcL16Params(InGamma -> nEntries, &L16In);
       InPtr  = InGamma -> GammaTable;

       cmsCalcL16Params(OutGamma -> nEntries, &L16Out);
       OutPtr = OutGamma-> GammaTable;

       for (i=0; i < nPoints; i++)
       {
              WORD wValIn, wValOut;

              wValIn  = cmsLinearInterpLUT16(_cmsQuantizeVal(i, nPoints), InPtr, &L16In);
              wValOut = cmsReverseLinearInterpLUT16(wValIn, OutPtr, &L16Out);

              p -> GammaTable[i] = wValOut;
       }

       return p;
}
// Reverse a gamma table

LPGAMMATABLE LCMSEXPORT cmsReverseGamma(int nResultSamples, LPGAMMATABLE InGamma)
{
       register int i;
       L16PARAMS L16In;
       LPWORD InPtr;
       LPGAMMATABLE p;

       p = cmsAllocGamma(nResultSamples);
       if (!p) return NULL;

       cmsCalcL16Params(InGamma -> nEntries, &L16In);
       InPtr  = InGamma -> GammaTable;

       for (i=0; i < nResultSamples; i++)
       {
              WORD wValIn, wValOut;

              wValIn = (WORD) ((int) ((int) i * 0xFFFFL) / (nResultSamples - 1));

              wValOut = cmsReverseLinearInterpLUT16(wValIn, InPtr, &L16In);
              p -> GammaTable[i] = wValOut;
       }

       return p;
}

LPGAMMATABLE cmsScaleGamma(LPGAMMATABLE Input, Fixed32 Factor)
{
       LPGAMMATABLE Output;
       int i, nEntries;


       nEntries = Input -> nEntries;
       Output = cmsAllocGamma(nEntries);
       if (!Output) return NULL;

       for (i=0; i < nEntries; i++)
              Output->GammaTable[i] = FixedScale(Input->GammaTable[i], Factor);

       return Output;
}


// Parameters goes as: Gamma, a, b, c, d, e, f

LPGAMMATABLE LCMSEXPORT cmsBuildParametricGamma(int nEntries, int Type, double Params[])
{
        LPGAMMATABLE Table;
        double R, Val;
        int i;

        Table = cmsAllocGamma(nEntries);
        if (NULL == Table) return NULL;


        for (i=0; i < nEntries; i++) {

                R   = (double) i / (nEntries-1);

                switch (Type) {

                // X = Y ^ Gamma
                case 0:
                      Val = pow(R, Params[0]);
                      break;

                // CIE 122-1966
                // Y = (aX + b)^Gamma + c | -b <= X <= a
                // Y = 0                  | else

                case 1:
                      if (R >= -Params[1] && R <= Params[2])
                              Val = pow(Params[1]*R + Params[2], Params[0]) + Params[3];
                      else
                              Val = 0;
                      break;

                // IEC 61966-3
                // Y = (aX + b)^Gamma + c | -b <= X <= a
                // Y = c                  | else

                case 2:
                      if (R >= -Params[1] && R <= Params[2])
                              Val = pow(Params[1]*R + Params[2], Params[0]) + Params[3];
                      else
                              Val = Params[3];
                      break;

                // IEC 61966-2.1 (sRGB)
                // Y = (aX + b)^Gamma | X >= d
                // Y = cX             | X < d

                case 3:
                      if (R >= Params[4])
                              Val = pow(Params[1]*R + Params[2], Params[0]);
                      else
                              Val = R * Params[3];
                      break;

                // Y = (aX + b)^Gamma + c | -b <= X <= a
                // Y = 0                  | else

                case 4:
                      if (R >= Params[4])
                              Val = pow(Params[1]*R + Params[2], Params[0]) + Params[3];
                      else
                              Val = R*Params[5] + Params[6];
                      break;

                default:
                        cmsSignalError(-1, "Unsupported parametrized curve");
                        cmsFreeGamma(Table);
                        return NULL;
                }


        Table->GammaTable[i] = (WORD) floor(Val * 0xFFFFL);
        }

        return Table;
}


// From: Eilers, P.H.C. (1994) Smoothing and interpolation with finite
// differences. in: Graphic Gems IV, Heckbert, P.S. (ed.), Academic
// press.
//
// Smoothing and interpolation with second differences.
//
//   Input:  weights (w), data (y): vector from 1 to m.
//   Input:  smoothing parameter (lambda), length (m).
//   Output: smoothed vector (z): vector from 1 to m.


#define MAX_KNOTS	4096
typedef float vec[MAX_KNOTS+1];

static
void smooth2(vec w, vec y, vec z, float lambda, int m)
{
  int i, i1, i2;
  vec c, d, e;
  d[1] = w[1] + lambda;
  c[1] = -2 * lambda / d[1];
  e[1] = lambda /d[1];
  z[1] = w[1] * y[1];
  d[2] = w[2] + 5 * lambda - d[1] * c[1] *  c[1];
  c[2] = (-4 * lambda - d[1] * c[1] * e[1]) / d[2];
  e[2] = lambda / d[2];
  z[2] = w[2] * y[2] - c[1] * z[1];
  for (i = 3; i < m - 1; i++) {
    i1 = i - 1; i2 = i - 2;
    d[i]= w[i] + 6 * lambda - c[i1] * c[i1] * d[i1] - e[i2] * e[i2] * d[i2];
    c[i] = (-4 * lambda -d[i1] * c[i1] * e[i1])/ d[i];
    e[i] = lambda / d[i];
    z[i] = w[i] * y[i] - c[i1] * z[i1] - e[i2] * z[i2];
  }
  i1 = m - 2; i2 = m - 3;
  d[m - 1] = w[m - 1] + 5 * lambda -c[i1] * c[i1] * d[i1] - e[i2] * e[i2] * d[i2];
  c[m - 1] = (-2 * lambda - d[i1] * c[i1] * e[i1]) / d[m - 1];
  z[m - 1] = w[m - 1] * y[m - 1] - c[i1] * z[i1] - e[i2] * z[i2];
  i1 = m - 1; i2 = m - 2;
  d[m] = w[m] + lambda - c[i1] * c[i1] * d[i1] - e[i2] * e[i2] * d[i2];
  z[m] = (w[m] * y[m] - c[i1] * z[i1] - e[i2] * z[i2]) / d[m];
  z[m - 1] = z[m - 1] / d[m - 1] - c[m - 1] * z[m];
  for (i = m - 2; 1<= i; i--)
     z[i] = z[i] / d[i] - c[i] * z[i + 1] - e[i] * z[i + 2];
}



// Smooths a curve sampled at regular intervals

BOOL LCMSEXPORT cmsSmoothGamma(LPGAMMATABLE Tab, double lambda)

{
	vec w, y, z;
	int i, nItems, Zeros, Poles;


	if (cmsIsLinear(Tab->GammaTable, Tab->nEntries)) return FALSE; // Nothing to do

    nItems = Tab -> nEntries;

    if (nItems > MAX_KNOTS) {
                cmsSignalError(LCMS_ERRC_ABORTED, "cmsSmoothGamma: too many points.");
				return FALSE;
                }

	ZeroMemory(w, nItems * sizeof(float));
	ZeroMemory(y, nItems * sizeof(float));
	ZeroMemory(z, nItems * sizeof(float));

	for (i=0; i < nItems; i++)
	{
		y[i+1] = (float) Tab -> GammaTable[i];
		w[i+1] = 1.0;
	}

	smooth2(w, y, z, (float) lambda, nItems);

	// Do some reality - checking...
	Zeros = Poles = 0;
	for (i=nItems; i > 1; --i) {

			if (z[i] == 0.) Zeros++;
			if (z[i] >= 65535.) Poles++;
			if (z[i] < z[i-1]) return FALSE; // Non-Monotonic
	}

	if (Zeros > (nItems / 3)) return FALSE;  // Degenerated, mostly zeros
	if (Poles > (nItems / 3)) return FALSE;	 // Degenerated, mostly poles

	// Seems ok

	for (i=0; i < nItems; i++) {

		// Clamp to WORD

		float v = z[i+1];

		if (v < 0) v = 0;
		if (v > 65535.) v = 65535.;

		Tab -> GammaTable[i] = (WORD) floor(v);
        }

	return TRUE;
}


// Check if curve is exponential, return gamma if so.

double LCMSEXPORT cmsEstimateGamma(LPGAMMATABLE t)
{
    double gamma, sum, sum2;
    double n, x, y, Std;
    int i;

    sum = sum2 = n = 0;
    
    // Does exclude endpoints
    for (i=1; i < t ->nEntries - 1; i++) {

            x = (double) i / (t -> nEntries - 1);
            y = (double) t -> GammaTable[i] / 65535.;
                    
            if (y > 0. && y < 1.) {

            gamma = log(y) / log(x);
            sum  += gamma;
            sum2 += gamma * gamma;
            n++;
            }
            
    }

    // Take a look on SD to see if gamma isn't exponential at all
    Std = sqrt((n * sum2 - sum * sum) / (n*(n-1)));
    

    if (Std > .7)
        return -1.0;

    return (sum / n);   // The mean
}



// -----------------------------------------------------------------Sampled curves

// Allocate a empty curve

LPSAMPLEDCURVE cmsAllocSampledCurve(int nItems)
{
	LPSAMPLEDCURVE pOut;

	pOut = (LPSAMPLEDCURVE) malloc(sizeof(SAMPLEDCURVE));
	if (pOut == NULL)
			return NULL;

	if((pOut->Values = (double *) malloc(nItems * sizeof(double))) == NULL)
	{
		free(pOut);
		return NULL;
	}

	pOut->nItems = nItems;
	ZeroMemory(pOut->Values, nItems * sizeof(double));

	return pOut;
}


void cmsFreeSampledCurve(LPSAMPLEDCURVE p)
{
	free((LPVOID) p -> Values);
	free((LPVOID) p);
}


// Take min, max of curve

void cmsEndpointsOfSampledCurve(LPSAMPLEDCURVE p, double* Min, double* Max)
{
        int i;

        *Min = 65536.;
        *Max = 0.;

        for (i=0; i < p -> nItems; i++) {

                double v = p -> Values[i];
                if (v < *Min)
                        *Min = v;

                if (v > *Max)
                        *Max = v;
        }
}

// Clamps to Min, Max

void cmsClampSampledCurve(LPSAMPLEDCURVE p, double Min, double Max)
{

        int i;

        for (i=0; i < p -> nItems; i++) {

                double v = p -> Values[i];

                if (v < Min)
                        v = Min;

                if (v > Max)
                        v = Max;

                p -> Values[i] = v;

        }

}


// Smooths a curve sampled at regular intervals

BOOL cmsSmoothSampledCurve(LPSAMPLEDCURVE Tab, double lambda)
{
	vec w, y, z;
	int i, nItems;

    nItems = Tab -> nItems;

    if (nItems > MAX_KNOTS) {
                cmsSignalError(LCMS_ERRC_ABORTED, "cmsSmoothSampledCurve: too many points.");
				return FALSE;
                }

	ZeroMemory(w, nItems * sizeof(float));
	ZeroMemory(y, nItems * sizeof(float));
	ZeroMemory(z, nItems * sizeof(float));

	for (i=0; i < nItems; i++)
	{
		y[i+1] = (float) Tab -> Values[i];
		w[i+1] = 1.0;
	}

	smooth2(w, y, z, (float) lambda, nItems);

	for (i=0; i < nItems; i++) {

		Tab -> Values[i] = z[i+1];;
     }

	return TRUE;

}


// Scale a value v, within domain Min .. Max 
// to a domain 0..(nPoints-1)

static
double ScaleVal(double v, double Min, double Max, int nPoints)
{

        double a, b;

        a = (double) (nPoints - 1) / (Max - Min);
        b = a * Min;

        return (a * v) - b;

}


// Does rescale a sampled curve to fit in a 0..(nPoints-1) domain

void cmsRescaleSampledCurve(LPSAMPLEDCURVE p, double Min, double Max, int nPoints)
{

        int i;

        for (i=0; i < p -> nItems; i++) {

                double v = p -> Values[i];

                p -> Values[i] = ScaleVal(v, Min, Max, nPoints);
        }

}

// Smooths a parametric curve giving a sampled one. X and Y *MUST* be monotonic

LPSAMPLEDCURVE cmsSmoothParametricCurve(LPSAMPLEDCURVE X, LPSAMPLEDCURVE Y,
												int nResultingPoints, double SmoothingLambda)
{

	vec w, y, z;
	int i, pos;
	LPSAMPLEDCURVE out;
    double MinX, MinY, MaxX, MaxY;

	out = cmsAllocSampledCurve(nResultingPoints);
    if (out == NULL)
		return NULL;

    if (X -> nItems != Y -> nItems) {

		cmsSignalError(LCMS_ERRC_ABORTED, "cmsSmoothParametricCurve: invalid curve.");
		cmsFreeSampledCurve(out);
		return NULL;
    }

	ZeroMemory(w, nResultingPoints * sizeof(float));
	ZeroMemory(y, nResultingPoints * sizeof(float));
	ZeroMemory(z, nResultingPoints * sizeof(float));

    cmsEndpointsOfSampledCurve(X, &MinX, &MaxX);
    cmsEndpointsOfSampledCurve(Y, &MinY, &MaxY);

	// Set our points, let smooth2 to interpolate missing ones.
	for (i=0; i < X -> nItems; i++) {

        pos      = (int) floor(ScaleVal(X -> Values[i], MinX, MaxX, nResultingPoints));
		y[pos+1] = (float) Y -> Values[i]; 
		w[pos+1] = 1.0;
	}

	smooth2(w, y, z, (float) SmoothingLambda, nResultingPoints);

	for (i=0; i < nResultingPoints; i++) {

                double v =z[i+1];
				out -> Values[i] = v;
    }

        
	return out;
}
