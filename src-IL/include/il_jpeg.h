//-----------------------------------------------------------------------------
//
// ImageLib Sources
// Copyright (C) 2000-2009 by Denton Woods
// Last modified: 03/07/2009
//
// Filename: src-IL/include/il_jpeg.h
//
// Description: Jpeg (.jpg) functions
//
//-----------------------------------------------------------------------------

#ifndef JPEG_H
#define JPEG_H

#include "il_internal.h"

/*
 * Application-specific, there are APP0, APP1... APP15, but like libjpeg documentation says: 
 * APP0, APP8 and APP14 shouldn't be replaced.
 */
#define JPEG_APP_MARKER (JPEG_APP0+12)


ILboolean iCheckJpg(ILubyte Header[2]);
ILboolean iIsValidJpg(void);

#ifndef IL_USE_IJL
	ILboolean iLoadJpegInternal(void);
	ILboolean iSaveJpegInternal(void);
#else
	ILboolean iLoadJpegInternal(ILconst_string FileName, ILvoid *Lump, ILuint Size);
	ILboolean iSaveJpegInternal(ILconst_string FileName, ILvoid *Lump, ILuint Size);
#endif

#endif//JPEG_H
